;;; ox-md-pandoc-zotero.el --- Export org citations to md and then to zotero formats via pandoc  -*- lexical-binding: t; -*-

;; Copyright (C) 2021  Anders Johansson

;; Author: Anders Johansson <mejlaandersj@gmail.com>
;; Created: 2021-06-23
;; Modified: 2022-02-14
;; Keywords: org, wp

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This org mode export backend allows exporting an org file using the
;; new org cite formats markdown and then via the pandoc lua filter
;; from zotero-better-bibtex to docx or odt files with active zotero
;; citations. It crucially depends on exporting citations correctly to
;; pandoc markdown, which can be achieved with the help of:
;; https://gitlab.com/andersjohansson/org-cite-pandoc
;; See customization options in group ‘ox-md-pandoc-zotero’

;;; Code:

(require 'org)
(require 'ox-md)

(defgroup ox-md-pandoc-zotero nil
  "Customization for org export to zotero via pandoc md."
  :group 'org-export)

(defcustom ox-md-pandoc-zotero-path-to-bbt-filter ""
  "Path to the betterbibtex pandoc filter for converting md to zotero odt/word.

Latest file should be here:
https://raw.githubusercontent.com/retorquere/zotero-better-bibtex/master/site/content/exporting/zotero.lua"
  :type 'file)

(defcustom ox-md-pandoc-zotero-format "odt"
  "Final export format. odt or docx."
  :type '(choice
          (const "odt")
          (const "docx")))

(defcustom ox-md-pandoc-zotero-style "apa"
  "File to set in zotero output file. A valid csl style name."
  :type 'string)

(defcustom ox-md-pandoc-zotero-extra-parameters ""
  "Extra parameters to pandoc. A string of valid parameters."
  :type 'string)

(org-export-define-derived-backend 'md-pandoc-zotero 'md
  :menu-entry
  '(?p "MD→pandoc zotero export"
       ((?z "As zotero WP file" ox-md-pandoc-zotero-wp)))
  :translate-alist
  '((table . ox-md-pandoc-zotero--table)
    (table-cell . org-org-identity)
    (table-row . org-org-identity)))

(defun ox-md-pandoc-zotero-wp (async subtreep visible-only body-only)
  "Export to md and then to odt with pandoc and zotero-better-bibtex filter."
  (interactive)
  (let ((outfile (org-export-output-file-name ".md" subtreep)))
    (org-export-to-file 'md-pandoc-zotero outfile async subtreep visible-only body-only nil
                        #'ox-md-pandoc-zotero-convert-to-wp)))

(defun ox-md-pandoc-zotero-convert-to-wp (file)
  "Convert markdown FILE to zotero odt or docx and open."
  (org-open-file
   (org-compile-file
    file
    (list
     (format
      "pandoc -s -o %%b.%s --lua-filter=%s --metadata=zotero_author_in_text=true --metadata=zotero_csl_style=%s %s --from markdown+pipe_tables+table_captions %%f"
      ox-md-pandoc-zotero-format
      ox-md-pandoc-zotero-path-to-bbt-filter
      ox-md-pandoc-zotero-style
      ox-md-pandoc-zotero-extra-parameters))
    ox-md-pandoc-zotero-format
    "See *ox-md-pandoc-zotero output* for details"
    (get-buffer-create "*ox-md-pandoc-zotero output*"))))

(defun ox-md-pandoc-zotero--table (table contents info)
  "Transcode TABLE back into Org syntax.
CONTENTS is its contents, as a string or nil. INFO is ignored."
  (let ((case-fold-search t)
        (caption (org-export-get-caption table)))
    (replace-regexp-in-string
     "^[ \t]*#\\+attr_[-_a-z0-9]+:\\(?: .*\\)?\n" ""
     (concat (org-export-expand table contents)
             (when caption
               (concat ": " (org-export-data caption info) "\n"))))))

(provide 'ox-md-pandoc-zotero)
;;; ox-md-pandoc-zotero.el ends here
